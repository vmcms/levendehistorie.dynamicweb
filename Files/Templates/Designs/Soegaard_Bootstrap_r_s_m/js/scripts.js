$(document).ready(function() {
    $('.bxslider').bxSlider({
        mode: 'fade',
        captions: false,
        auto: true,
        speed: 1200,
        randomStart: false,
        pause: 5000,
 
    });
    $('#section-top.bxslider').bxSlider({
        mode: 'fade',
        captions: false,
        auto: true,
        speed: 1200,
        randomStart: false,
        pause: 7000
    });

    var max_height = 0;
    $(".front-news").each(function() {
        if ($(this).height() > max_height) {
            max_height = $(this).height();
        }
    });
    $(".front-news").height(max_height); 

    $("#mc-embedded-subscribe-form").submit(function(e) {
        e.preventDefault();

        var name = $("#mce-NAME").val();
        var mail = $("#mce-EMAIL").val();
        var list = $("#mce-LIST").val();

        var url = "/Admin/Public/Mailchimp/mailchimp.cshtml?&email=" + mail + "&name=" + name + "&list=" + list;
        $.ajax({url: url, async: false, success: function(result){}});

        window.location = '/nyhedsbrev-tilmelding/kvittering';
    });
});